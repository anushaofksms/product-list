import { Component ,OnInit} from '@angular/core';
import { ListService } from './list.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker/public_api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  
  datePickerConfig: Partial<BsDatepickerConfig>
  title = 'product-list';
  list:any;
  empId:any;
  empName:any;
  empSalary:any;
  active:boolean;
  addForm:boolean;
  editForm:boolean;
  empDob:any;
  indexId:number;
  name:string;
  fetchData=
  {
    'id':"",
    'name':"",
    'salary':"",
    'dob':""
  }

  constructor (private listService:ListService)
  {
    this.list=[];
    this.active = false
    this.addForm = false
    this.editForm = false

    this.datePickerConfig = Object.assign({},
      {
        containerClass: 'theme-dark-blue',
        showWeekNumbers: true,
        minDate: new Date(2019, 0, 1),
        maxDate: new Date(2019, 11, 31),
        dateInputFormat: 'DD/MM/YYYY'
      });
  }

  ngOnInit() {
    this.getAll();
  }

  getAll()
  {
    try {
      this.listService.getAll()
        .subscribe(response => {
          this.list=response
          // console.log(this.list);
        }, error => {
          console.log('==Get error Details==', error)
        })
    } catch (ex) {
      console.log(ex)
    }
  }

  listData(data,i)
  {
    this.active = true;
    this.empId = data.id,
    this.empName = data.name,
    this.empSalary = data.salary,
    this.empDob = data.dob,
    this.indexId = i,
    this.editForm = false;
  }

  addData()
  { 
     this.addForm = true;
  }

  addUser(list)
  {
    let data = {
      id:list.id,
      name:list.name,
      salary:list.salary,
      dob:list.dob,
    }
    this.list.push(data);
    this.list;
    this.addForm = false;
  }

  editUser(edit)
  {
    console.log(edit.id);
    for (let index = 0; index < this.list.length; index++) {
      if(this.list[index].id == edit.id)
      {
        this.list[index].name = edit.name;
        this.list[index].salary = edit.salary;
      }
    }
    this.editForm = false;
    this.list;
  }

  editClick()
  {
    this.addForm = false;
    this.editForm = true;
    this.active = false;
    this.fetchData.id = this.empId;
    this.fetchData.name = this.empName;
    this.fetchData.salary = this.empSalary;
    this.fetchData.dob = this.empDob;
  }

  deleteClick()
  {
     this.list.splice(this.indexId,1);
     this.active = false;
  }

  search()
  {
    if(this.name != "")
    {
      this.list = this.list.filter(res => {
        return res.name.toLocaleLowerCase().match(this.name.toLocaleLowerCase());
      });
    }

    else if(this.name == "")
    {
      this.ngOnInit();
    }
  }
}
